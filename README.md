# Python Configuring EC2 Server Instancs 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

Wrote a Python script that automates adding environment tags to all EC2 Server instances 

## Technologies Used

* Python 

* Boto3

* AWS 

## Steps 

Step 1: Create new file for python code 

[Python file created](/images/01_create_new_file_for_python_code.png)

Step 2: Import boto3 library to file 

[Import boto3](/images/02_import_boto3_library_to_file.png)

Step 3: Call boto3 client function and insert appropriate parameters, when done assign it to a variable 

[boto3 client](/images/04_insert_describe_instances_function_to_the_variable_for_the_client_and_save_it_in_a_variable.png)

Step 4: Create a for loop to allow program loop through the reservation list to find instances 

[loop through reservation list](/images/05_create_a_for_loop_to_allow_program_loop_through_the_reservations_list_to_find_instances.png)

Step 5: Create another for loop to loop through instances 

[loop through instances](/images/06_create_another_for_loop_to_loop_through_instances.png)

Step 6: Create an empty list to store instances

[Create list](/images/07_create_an_empty_list_store_instances.png)

Step 7: Append instance id to empty list in for loop

[Append](/images/08_add_append_instance_id_to_empty_list_in_for_loop.png)

Step 8: Call resource function for creation of tags and save to variable 

[Resource](/images/09_call_resource_function_for_creation_tags_and_save_to_variable.png)

Step 9: Call function create tag resource

[Create tag resource](/images/10_call_function_create_tag_resource.png)

Step 10: Call client and resource function for frankfurt region

[Frankfurt region](/images/11_call_client_and_resource_function_for_frankfurt_region.png)

Step 11: Create empty list for ec2 instance ids in frankfurt

[Create Empty list](/images/12_create_empty_list_for_ec2_instance_ids_in_frankfurt.png)

Step 12: Copy the same logic used to loop and tag in paris region

[Copy code](/images/13_copy_the_same_logic_used_to_loop_and_tag_in_paris_region.png)

Step 13: Run program 

[Run program](/images/14_run_program.png)

[Paris tags](/images/15_paris_tags.png)
[Frankfurt tags](/images/16_frankfurt_tags.png)

## Installation

    brew install python3


## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/python-configuring-ec2-server-instances.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/python-configuring-ec2-server-instances

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.